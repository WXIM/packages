# [ctrlc-git 博客](https://ctrlc-git.gitee.io/)

| 版本要求 | 描述 |
| -------- | ---- |
| node     | >=14.0.0 或者>=16.0.0" |
| pnpm     | >=7.14.0 |

## 插件目录

插件|描述|仓库
--|--|---
[hook_push](https://gitee.com/ctrlc-git/ctrlc-npm-cli-push.git)|语义化修改版本号，可选`tag`
[storage](https://gitee.com/ctrlc-git/ctrlc-web-storage.git)| 网页缓存封装
[uni-module-pages](https://gitee.com/ctrlc-git/ctrlc-uni-module-pages.git)| `uniapp`项目处理`pages.json`的模块化路由插件
[vite-plugin-vue-html-insert](https://gitee.com/ctrlc-git/ctrlc-vite-html-insert.git)|用于`vue3`在`index.html`注入`css`、`js`
[vite-plugin-vue-setup-extend](https://gitee.com/ctrlc-git/ctrlc-vite-setup-extend.git)| `vue3`在`<script setup>`语法时，扩展`name`以及`inheritAttrs`属性
[mouse-track](https://www.npmjs.com/package/@ctrlc/mouse-track)| webGL动画鼠标轨迹 |[示例](https://ctrlc-git.gitee.io/example/mouse-track/)|
[vite-plugin-vue-svgo](https://www.npmjs.com/package/@ctrlc/vite-plugin-vue-svgo)| 利用`svgo`批量处理`svg`文件
