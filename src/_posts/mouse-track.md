---
title: mouse-track
date: 2023-08-26 01:02:13
tags: canvas
categories: 
- 前端
- canvas
excerpt: 鼠标轨迹动画。
---

## 使用手册

### 安装

```bash
npm i @ctrlc/mouse-track -S
```

### 语法

```JS
new MouseTrack(element, options);
```

### 参数

参数|类型|说明
----|----|----
element| HTMl标准属性 **HTMLCanvasElement** |必填|
options|HTMLCanvasElement|配置属性 TODO

### 示例

```html
<canvas id="canvas" ref="elCanvas"></canvas>
```

```js
// vue
import MouseTrack from '@ctrlc/mouse-track';
const elCanvas = ref();
onMounted(() => {
  new MouseTrack(elCanvas.value);
});

// js
const canvas = document.getElementById('canvas');
new MouseTrack(canvas);
```

## 效果页面

[MouseTrack](/example/mouse-track/)
