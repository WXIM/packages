---
title: 人工智能手势识别
date: 2023-09-20:10:20
tags: ['机器学习','深度学习']
categories: 
- 人工智能
- 深度学习
excerpt: paddlejs深度学习识别图片中人物手掌区域。
---

## 效果

![手势识别](https://gitee.com/ctrlc-git/paddlejs/raw/master/public/demo.png)

## 效果页面

- 开启摄像头权限

[演示地址](https://ctrlc-git.gitee.io/paddlejs/)
