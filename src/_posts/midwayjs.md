---
title: 【midwayjs系列】项目搭建
date: 2024-03-11 13:55:39
tags: [midwayjs,全栈]
categories: 
- 全栈
- Midwayjs
excerpt: 一款基于MidwayJS的管理后台，前后端分离。
---

## 项目架构

* 前端采用vite、vue、elementplus、typescript
* 后端采用midwayjs
* 项目采用pnpm搭建

## 项目结构

```bash
├─packages
|  ├─client # 客服端项目
|  |   ├─package.json
|  ├─server # 服务端项目
|  |   ├─package.json
```

### 参考资料

[vue 文档][vuejs]。
[midway 文档][midway]。

[vuejs]: https://vuejs.org/
[midway]: https://midwayjs.org
